data "aws_caller_identity" "default" {}
data "aws_iam_account_alias" "current" {}

locals {
  alarm_name_prefix = "[${title(var.alarm_name_prefix == "" ? data.aws_iam_account_alias.current.account_alias : var.alarm_name_prefix)}]"
}

resource "aws_cloudwatch_metric_alarm" "burst_balance_too_low" {
  alarm_name          = "${title(local.alarm_name_prefix)} efs-${var.efs_id}-lowBurstBalance"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "BurstCreditBalance"
  namespace           = "AWS/EFS"
  period              = var.statistic_period
  statistic           = "Average"
  threshold           = "100000000000"
  alarm_description   = "Average burst credit balance is low, a performance impact will occur within the hour."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    FileSystemId = var.efs_id
  }
}

resource "aws_cloudwatch_metric_alarm" "io_percentage_too_high" {
  alarm_name          = "${title(local.alarm_name_prefix)} efs-${var.efs_id}-highIOPercentage"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = var.evaluation_period
  metric_name         = "PercentIOLimit"
  namespace           = "AWS/EFS"
  period              = var.statistic_period
  statistic           = "Maximum"
  threshold           = "90"
  alarm_description   = "I/O limit has been reached, consider using Max I/O performance mode."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    FileSystemId = var.efs_id
  }
}

resource "aws_cloudwatch_metric_alarm" "throughput_utilization" {
  alarm_name          = "${title(local.alarm_name_prefix)} efs-${var.efs_id}-ThroughputUtilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.evaluation_period
  threshold           = "75"
  alarm_description   = "Throughput Utilization is high."
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  metric_query {
    id         = "e1"
    expression = "(m1/1048576)/PERIOD(m1)"
    label      = "Expression1"
  }

  metric_query {
    id         = "e2"
    expression = "m2/1048576"
    label      = "Expression2"
  }

  metric_query {
    id         = "e3"
    expression = "e2-e1"
    label      = "Expression3"
  }

  metric_query {
    id          = "e4"
    expression  = "((e1)*100)/(e2)"
    label       = "Throughput utilization (%)"
    return_data = "true"
  }

  metric_query {
    id = "m1"
    metric {
      metric_name = "TotalIOBytes"
      namespace   = "AWS/EFS"
      period      = "60"
      stat        = "Sum"
      dimensions = {
        FileSystemId = var.efs_id
      }
    }
  }

  metric_query {
    id = "m2"
    metric {
      metric_name = "PermittedThroughput"
      namespace   = "AWS/EFS"
      period      = "60"
      stat        = "Sum"

      dimensions = {
        FileSystemId = var.efs_id
      }
    }
  }
}
